Entity reference trim 
-----------------------------------------------------------------------------

	Entity reference trim module is a small and simple module for Drupal 7 which 
	allows you to hide entity target id for autocomplete widget type 
	(referenced target id between two brackets). 
	Example of usage:
	Lets say that you have autocomplete entityreference field for cars content 
	type, selected value for that field is present as "Audi (5)", or "BMW (8)". 
	This module simply hide terget id and selected value looks: "Audi" or "BMW".
      
Notes     
-----------------------------------------------------------------------------
	Module does not support autocomplete tag style widget type for entity
	reference field. 
