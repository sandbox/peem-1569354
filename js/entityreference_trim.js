(function ($) {
  $(window).load(function(){
    var entityIds = {};

    Drupal.jsAC.prototype.hidePopup = function (keycode) {
      if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
        var contents = $(this.selected).data('autocompleteValue');

        if ($(this.selected).parents('.field-type-entityreference').length > 0) {
        	var match = contents.match(/\((.*?)\)$/);
        	contents = contents.replace(match[0], '');

        	var elementId = this.input.id;
        	entityIds[elementId] = match[1];
        	console.log(entityIds[this.input.id]);

	        $('<input>').attr({
    	      type: 'hidden',
        	  id: this.input.id,
    		  name: this.input.id,
    	  	  value: entityIds[this.input.id]
			}).appendTo('form');

    	}
    	this.input.value = contents;
      }

      var popup = this.popup;
      if (popup) {
        this.popup = null;
        $(popup).fadeOut('fast', function () { $(popup).remove(); });
      }
      this.selected = false;
      $(this.ariaLive).empty();
   };
  });
}(jQuery));
